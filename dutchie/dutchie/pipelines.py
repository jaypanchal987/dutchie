# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
import datetime

from itemadapter import ItemAdapter
import pymysql
from dutchie import db_config
from dutchie.items import DutchieProducerItem, DutchieProductItem
import pandas as pd
from scrapy.exporters import CsvItemExporter


class DutchiePipeline:
    def open_spider(self, spider):
        try:
            self.con = pymysql.connect(db_config.host, db_config.user, db_config.password)
            self.cursor = self.con.cursor()
            self.cursor.execute(f'CREATE DATABASE IF NOT EXISTS {db_config.databse}')
        except Exception as e:
            print(e)

        self.con = pymysql.connect(db_config.host, db_config.user, db_config.password, db_config.databse)
        self.cursor = self.con.cursor()

        try:
            create = f"""CREATE TABLE IF NOT EXISTS {db_config.producer_table} (`Id` int NOT NULL AUTO_INCREMENT,
                                                                        Producer_ID varchar(255),
                                                                        Producer varchar(255),
                                                                        Description text,   
                                                                        Link varchar(255),
                                                                        SKU varchar(255),
                                                                        City varchar(255),
                                                                        Province varchar(255),
                                                                        ccc varchar(255),
                                                                        Page_Url varchar(255),
                                                                        Active varchar(255),
                                                                        Main_image varchar(255),
                                                                        Image_2 varchar(255),
                                                                        Image_3 varchar(255),
                                                                        Image_4 varchar(255),
                                                                        Image_5 varchar(255),
                                                                        `Type` varchar(255),
                                                                        License_Type varchar(255),
                                                                        Date_Licensed varchar(255),
                                                                        Phone varchar(255),
                                                                        Phone_2 varchar(255),
                                                                        Contact_Name varchar(255),
                                                                        EmailPrivate varchar(255),
                                                                        Email varchar(255),
                                                                        Social varchar(255),
                                                                        Address varchar(255),
                                                                        Additional_Info varchar(255),
                                                                        Created datetime,
                                                                        Updated datetime,
                                                                        Status varchar(10) NOT NULL DEFAULT "Pending",
                                                                        primary key(`Id`),
                                                                        unique key(Producer_ID));"""
            self.cursor.execute(create)
        except Exception as e:
            print(e)


        try:
            export_order = [
                "Producer_ID", "Page_URL", "Brand", "Name", "SKU", "Out_stock_status", "Currency", "ccc", "Price",
                "Manufacturer", "Main_image", "Description", "Product_ID", "Additional_Information", "Meta_description",
                "Meta_title", "Old_Price", "Equivalency_Weights", "Quantity", "Weight", "Option", "Option_type",
                "Option_Value", "Option_image", "Option_price_prefix", "Cat_tree_1_parent", "Cat_tree_1_level_1",
                "Cat_tree_1_level_2", "Cat_tree_2_parent", "Cat_tree_2_level_1", "Cat_tree_2_level_2",
                "Cat_tree_2_level_3", "Image_2", "Image_3", "Image_4", "Image_5", "Sort_order", "Attribute_1",
                "Attribute_Value_1", "Attribute_2", "Attribute_value_2", "Attribute_3", "Attribute_value_3",
                "Attribute_4", "Attribute_value_4", "Reviews", "Review_link", "Rating"
            ]

            current_date = datetime.datetime.now().strftime('%Y%m%d_%H%M')
            path = spider.output_path + f'Product_{str(current_date)}.csv'
            self.csv_file_name = open(path, "wb")
            self.csv_file_exporter = CsvItemExporter(self.csv_file_name)
            self.csv_file_exporter.fields_to_export = export_order
            self.csv_file_exporter.start_exporting()
        except Exception as e:
            print(e)

    def process_item(self, item, spider):
        if isinstance(item, DutchieProducerItem):
            try:
                insert = f"""insert into {db_config.producer_table}(p_id, producer, description, link, SKU, city, province, ccc, url, active, type, licenseType, dateLicensed, phone, phone2, email, productCount, image, image2, image3, image4, image5, contactName, emailPrivate, address, additionalInfo) values (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
                self.cursor.execute(insert, (
                    item["Producer_ID"], item["Producer"], item["Description"], item["Link"], item["SKU"], item["City"], item["Province"],
                    item["ccc"], item["Page_Url"], item["Active"], item["Type"], item["License_Type"], item["Date_Licensed"],
                    item["Phone"], item["Phone_2"], item["Email"], "", item["Main_image"],
                    item["Image_2"], item["Image_3"], item["Image_4"], item["Image_5"], item["Contact_Name"],
                    item["EmailPrivate"], item["Address"], item["Additional_Info"]))
                self.con.commit()
                print('Producer Inserted...')
            except Exception as e:
                print(e)

        if isinstance(item, DutchieProductItem):
            try:
                self.csv_file_exporter.export_item(item)
            except Exception as e:
                print(e)

        return item

    def close_spider(self, spider):
        try:
            current_date = datetime.datetime.now().strftime('%Y%m%d_%H%M')
            if spider.name == 'get_producers':
                path = spider.output_path + f'Producers_{spider.city}_{str(current_date)}.csv'
                select = f'select * from {db_config.producer_table}'
                df = pd.read_sql(select, self.con)
                df.to_csv(path, index=False)
                print('Generated...', path)

            if spider.name == 'get_products':
                self.csv_file_exporter.finish_exporting()
                self.csv_file_name.close()
                print('Generated...')
        except Exception as e:
            print(e)