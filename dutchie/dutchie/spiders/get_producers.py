import datetime
import json
import os
import re

from dutchie.items import DutchieProducerItem
import scrapy


class GetProducersSpider(scrapy.Spider):
    name = 'get_producers'
    allowed_domains = []
    can_province_abbrev = {
        'Alberta': 'AB',
        'British Columbia': 'BC',
        'Manitoba': 'MB',
        'New Brunswick': 'NB',
        'Newfoundland and Labrador': 'NL',
        'Northwest Territories': 'NT',
        'Nova Scotia': 'NS',
        'Nunavut': 'NU',
        'Ontario': 'ON',
        'Prince Edward Island': 'PE',
        'Quebec': 'QC',
        'Saskatchewan': 'SK',
        'Yukon': 'YT'
    }

    def __init__(self, city='', **kwargs):
        super().__init__(**kwargs)
        self.city = city.lower()

        cwd = os.getcwd()
        self.output_path = cwd + '\\output\\'
        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)

    def start_requests(self):
        try:
            url = f"https://nominatim.openstreetmap.org/search?q={self.city}&format=json"
            yield scrapy.Request(
                url=url
            )
        except Exception as e:
            print(e)

    def parse(self, response):
        try:
            data = json.loads(response.body)
            lat = data[0]["lat"]
            lng = data[0]["lon"]
            display_name = data[0]["display_name"].split(',')[-3].strip()
            state = self.can_province_abbrev[display_name]
            search_url = 'https://dutchie.com/graphql?operationName=DispensarySearch&variables={"dispensaryFilter":{"type":"Dispensary","activeOnly":true,"nearLat":'+str(lat)+',"nearLng":'+str(lng)+',"destinationTaxState":"'+str(state)+'","recreational":true,"medical":true,"creditCards":false,"distance":35}}&extensions={"persistedQuery":{"version":1,"sha256Hash":"a10394dbe2f750a88e52f36a318c8c3cc445c8e6bdce8de713a23b660041f7dd"}}'
            yield scrapy.Request(
                url=search_url,
                callback=self.parse_producers
            )
        except Exception as e:
            print(e)

    def parse_producers(self, response):
        try:
            producer_data = json.loads(response.body)
            for dispen in producer_data["data"]["filteredDispensaries"]:
                Producer_ID = dispen["id"]
                Producer = dispen["name"]
                Description = ''
                Link = 'https://dutchie.com/'
                SKU = ''
                City = ''
                Province = ''
                ccc = ''
                Page_Url = 'https://dutchie.com/dispensaries/' + dispen["cName"]
                Active = '1'
                Main_image = dispen["listImage"]
                if not Main_image:
                    Main_image = 'https://neobi.io/images/coming-soon.jpg'
                Image_2 = ''
                Image_3 = ''
                Image_4 = ''
                Image_5 = ''
                Type = dispen["__typename"]
                if not Type:
                    Type = 'Unknown'
                License_Type = ''
                Date_Licensed = ''
                Phone = ''
                Phone_2 = ''
                Contact_Name = ''
                EmailPrivate = ''
                Email = ''
                Social = ''
                Address = ''
                Additional_Info = ''
                Created = datetime.datetime.today()
                Updated = None

                item = DutchieProducerItem()
                item["Producer_ID"] = Producer_ID
                item["Producer"] = Producer
                item["Description"] = Description
                item["Link"] = Link
                item["SKU"] = SKU
                item["City"] = City
                item["Province"] = Province
                item["ccc"] = ccc
                item["Page_Url"] = Page_Url
                item["Active"] = Active
                item["Main_image"] = Main_image
                item["Image_2"] = Image_2
                item["Image_3"] = Image_3
                item["Image_4"] = Image_4
                item["Image_5"] = Image_5
                item["Type"] = Type
                item["License_Type"] = License_Type
                item["Date_Licensed"] = Date_Licensed
                item["Phone"] = Phone
                item["Phone_2"] = Phone_2
                item["Contact_Name"] = Contact_Name
                item["EmailPrivate"] = EmailPrivate
                item["Email"] = Email
                item["Social"] = Social
                item["Address"] = Address
                item["Additional_Info"] = Additional_Info
                item["Created"] = Created
                item["Updated"] = Updated
                yield item
        except Exception as e:
            print(e)