import json
import os

from dutchie.items import DutchieProductItem
import scrapy
import pymysql
from dutchie import db_config


class GetProductsSpider(scrapy.Spider):
    name = 'get_products'
    allowed_domains = []

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        cwd = os.getcwd()
        self.output_path = cwd + '\\output\\'
        if not os.path.exists(self.output_path):
            os.makedirs(self.output_path)

        self.con = pymysql.connect(db_config.host, db_config.user, db_config.password, db_config.databse)
        self.cursor = self.con.cursor()

    def start_requests(self):
        try:
            select = f'select p_id,url from {db_config.producer_table}'
            self.cursor.execute(select)
            producers = self.cursor.fetchall()
            for producer in producers:
                if producer[0]:
                    Producer_ID = producer[0]
                    Page_Url = producer[1]
                    url = 'https://dutchie.com/graphql?operationName=FilteredProducts&variables={"productsFilter":{"dispensaryId":"'+str(Producer_ID)+'","bypassOnlineThresholds":false},"useCache":true}&extensions={"persistedQuery":{"version":1,"sha256Hash":"f9aa416373c389619a5bb8cf0cd5819a5eb605b3762c5404ea128021ede6658a"}}'
                    yield scrapy.Request(
                        url=url,
                        meta={
                            'Producer_ID': Producer_ID,
                            'Page_Url': Page_Url
                        }
                    )
        except Exception as e:
            print(e)

    def parse(self, response):
        try:
            Page_Url = response.meta["Page_Url"]
            Producer_ID = response.meta["Producer_ID"]
            product_data = json.loads(response.body)
            for product in product_data["data"]["filteredProducts"]["products"]:
                item = DutchieProductItem()
                item["Producer_ID"] = Producer_ID
                item["Product_ID"] = product['id']
                item["Page_URL"] = Page_Url + '/menu/' + product["cName"]
                item["Brand"] = product["brandName"]
                item["Name"] = product["Name"]
                item["SKU"] = ''
                item["Out_stock_status"] = product["Status"]
                item["Currency"] = ''
                item["ccc"] = ''
                item["Price"] = product["Prices"][0]
                item["Manufacturer"] = 'https://dutchie.com/'
                item["Main_image"] = product["Image"]
                item["Description"] = ''
                item["Additional_Information"] = ''
                item["Meta_description"] = ''
                item["Meta_title"] = ''
                item["Old_Price"] = ''
                item["Equivalency_Weights"] = product["Options"][0]
                item["Quantity"] = 0#product["POSMetaData"]["children"][0]["quantityAvailable"]
                item["Weight"] = ''
                item["Option"] = ''
                item["Option_type"] = ''
                item["Option_Value"] = ''
                item["Option_image"] = ''
                item["Option_price_prefix"] = ''
                item["Cat_tree_1_parent"] = product["type"]
                item["Cat_tree_1_level_1"] = product["subcategory"] if product["subcategory"] else ''
                item["Cat_tree_1_level_2"] = ''
                item["Cat_tree_2_parent"] = ''
                item["Cat_tree_2_level_1"] = ''
                item["Cat_tree_2_level_2"] = ''
                item["Cat_tree_2_level_3"] = ''
                item["Image_2"] = ''
                item["Image_3"] = ''
                item["Image_4"] = ''
                item["Image_5"] = ''
                item["Sort_order"] = ''
                item["Attribute_1"] = 'THC'
                item["Attribute_Value_1"] = []
                if product["THCContent"]:
                    if product["THCContent"]["range"]:
                        for THC in product["THCContent"]["range"]:
                            if THC:
                                item["Attribute_Value_1"].append(str(THC))
                item["Attribute_Value_1"] = ' - '.join(item["Attribute_Value_1"])
                item["Attribute_2"] = 'CBD'
                item["Attribute_value_2"] = []
                if product["CBDContent"]:
                    if product["CBDContent"]["range"]:
                        for CBD in product["CBDContent"]["range"]:
                            if CBD:
                                item["Attribute_value_2"].append(str(CBD))
                item["Attribute_value_2"] = ' - '.join(item["Attribute_value_2"])
                item["Attribute_3"] = 'Type'
                item["Attribute_value_3"] = product["type"]
                item["Attribute_4"] = ''
                item["Attribute_value_4"] = ''
                item["Reviews"] = ''
                item["Review_link"] = ''
                item["Rating"] = ''
                url = 'https://dutchie.com/graphql?operationName=ProductsQuery&variables={"productsFilter":{"productId":"'+item["Product_ID"]+'","bypassOnlineThresholds":false},"hideEffects":false,"useCache":true}&extensions={"persistedQuery":{"version":1,"sha256Hash":"ecafc18ce358baa98e9882f8f6b514a97d8559faef9130501b92164df7411e4c"}}'
                yield scrapy.Request(
                    url=url,
                    callback=self.parse_products,
                    meta={
                        'item': item
                    }
                )
        except Exception as e:
            print(e)


    def parse_products(self, response):
        try:
            item = response.meta["item"]
            data = json.loads(response.body)
            Description = data["data"]["filteredProducts"]["products"][0]["Description"]
            Meta_description = data["data"]["filteredProducts"]["products"][0]["brandDescription"]
            Additional_Information = json.dumps(data["data"]["filteredProducts"]["products"][0]["effects"])
            item["Description"] = Description
            item["Meta_description"] = Meta_description
            item["Additional_Information"] = Additional_Information
            yield item

        except Exception as e:
            print(e)